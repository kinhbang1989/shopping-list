export const theme = {
  primaryColor: 'rgba(32,33, 37,1)',
  primaryFontColor: '#202125',
  buttonBackground: '#cacaca',
  borderColor: '#000000'
}
