
import {generateRandomProducts} from '../../utils';
import React from 'react';
import ShoppingList, {TitileWrapper, ListWrapper, AddProductWrapper} from './';
import {shallow} from 'enzyme';
import map from 'lodash/map';
function getwrapper(props) {
  return shallow(<ShoppingList {...props} />);
}
describe('ShoppingList', () => {
  it('render correctly structure of the app', () => {
    const wrapper = getwrapper();
    expect(wrapper.find(TitileWrapper).length).toEqual(1);
    expect(wrapper.find(ListWrapper).length).toEqual(1);
    expect(wrapper.find(AddProductWrapper).length).toEqual(1);
  });
  it('addProduct will add new product to state products', () => {
    const products = generateRandomProducts();
    const expectedProduct = {id: 123, name: 'sample 1', amount: 1}
    const wrapper = getwrapper();
    wrapper.setState({products});
    wrapper.instance().addProduct(expectedProduct);
    const stateProducts = wrapper.state().products;
    expect(stateProducts.length).toEqual(products.length + 1)
    expect(stateProducts[stateProducts.length - 1]).toEqual(expectedProduct)
  });
  it('updateProduct will update the right one', () => {
    const products = generateRandomProducts();
    const expectedUpdateIndex = 0;
    const expectedUpdateObject = {amount: 1000, name: 'fiifafo'};
    const wrapper = getwrapper();
    wrapper.setState({products})
    wrapper.instance().updateProduct(expectedUpdateIndex, expectedUpdateObject);
    const stateProducts = wrapper.state().products;
    expect(stateProducts[expectedUpdateIndex]).toEqual({id: stateProducts[expectedUpdateIndex].id, ...expectedUpdateObject})
  });
  it('removeProduct will removeProduct with right id', () => {
    const products = [{id: 1}, {id: 2}, {id: 3}];
    const expectedRemoveProductId = 1;
    const wrapper = getwrapper();
    wrapper.setState({products})
    wrapper.instance().removeProduct(expectedRemoveProductId);
    const stateProducts = wrapper.state().products;
    expect(stateProducts.length).toEqual(products.length - 1)
    expect(map(stateProducts, 'id')).not.toContain(expectedRemoveProductId)
  })
});
